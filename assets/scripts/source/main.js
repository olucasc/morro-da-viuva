(function($) {

	/* Functions
	------------------------ */

	//Detectar usuário de teclado
	function handleFirstTab(e) {
	    if (e.keyCode === 9) { // detecta o primeiro tab do usuário
	    	document.body.classList.add('user-is-tabbing');
	    	window.removeEventListener('keydown', handleFirstTab);
	    }
	}
	// Play Video
	function playVideo( $el ) {

		var videoCode = $el.attr( 'data-id' ),
		player;

		if( typeof( YT ) !== 'undefined' ) {

			$el.addClass( 'play' );
			player = new YT.Player( videoCode, {
				videoId: videoCode,
				playerVars: {
					rel: 0,
					autoplay: 1,
					enablejsapi: 1
				}
			});

		}

	}

	/* Triggers
	------------------------ */
	// Document Ready
	$(document).ready( function() {

		//Galeria de imagens
		$('.site-view_gallery').slick({
			dots: false,
			infinite: false,
			speed: 500,
		});

	});

	//Trigger de usuário de teclado
	window.addEventListener('keydown', handleFirstTab);

	// Play Video
	

	// Clique Scroll Down

	// Teste scroll
	// new PerfectScrollbar('.container-ps');
	// new PerfectScrollbar('.bg-block');
	// new PerfectScrollbar('#container');
	// alert('Teste');


	var section_visible = 1;

$(document).on('click', '.nav__dot', function(){
    var section = $(this).data('section');
    var speed = Math.abs(section_visible - section);
    section_visible = section;

    $('html, body').animate({
        scrollTop: $('#'+section).offset().top
    }, 400*speed);
});

$(document).on('click', '.arrow-down', function(){
    $('html, body').animate({
        scrollTop: $('#2').offset().top
    }, 400);
});


$(window).scroll(function(){
	if($(this).scrollTop()<=$('#2').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=1]').addClass('on');

		$('.nav__dots').removeClass('negative');
	}else if($(this).scrollTop()<=$('#3').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=2]').addClass('on');

		$('.nav__dots').removeClass('negative');
		$('.nav__dots').addClass('negative');
	}else if($(this).scrollTop()<$('#4').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=3]').addClass('on');

		$('.nav__dots').removeClass('negative');
	}else if($(this).scrollTop()<$('#5').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=4]').addClass('on');

		$('.nav__dots').removeClass('negative');
		$('.nav__dots').addClass('negative');
	}else if($(this).scrollTop()<$('#6').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=5]').addClass('on');

		$('.nav__dots').removeClass('negative');
	}else if($(this).scrollTop()<$('#7').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=6]').addClass('on');

		$('.nav__dots').removeClass('negative');
		$('.nav__dots').addClass('negative');
	}else if($(this).scrollTop()<$('#8').position().top-1){
		$('.nav__dot').removeClass('on');
		$('.nav__dot[data-section=7]').addClass('on');
	}
});


})(jQuery);